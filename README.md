# Automation scripts

My collection of automation scripts :computer: created over the last couple of years.

## Intro

Bash and Python automation scripts for Linux kernel based OS and AWS public cloud services.

## Overview

*   [x] **Can be modified and extended**
*   [x] **Tested**

### Requirements

* Bash
* Python 3.10

### Install

Use git to clone this repository.

### Usage

Bash scripts: ./scriptname.sh

Python: python scriptname.py
