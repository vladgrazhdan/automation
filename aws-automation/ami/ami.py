#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : ami.py
# desc    : Filtering and deleting AMIs
# time    : 2023/09/22 15:03:14
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''

import boto3
import csv
from datetime import datetime
from time import sleep
from vars import *


def ami_list(**parameters):

    # Assuming role to an account from vars file
    for account in parameters['accounts']:
        sts_client = boto3.client('sts')

        print(f"Assuming {parameters['role']} Role to {account} account...")
        sleep(2)

        assumed_role_object = sts_client.assume_role(
            RoleArn=f"arn:aws:iam::{parameters['accounts'][account]}:role/{parameters['role']}",
            RoleSessionName="AssumeRoleSession1")
        credentials = assumed_role_object['Credentials']

        # Fetching private AMIs IDs in Sydney region
        client = boto3.client('ec2',
                              region_name='ap-southeast-2',
                              aws_access_key_id=credentials['AccessKeyId'],
                              aws_secret_access_key=credentials['SecretAccessKey'],
                              aws_session_token=credentials['SessionToken'],)
        ami_list_raw = client.describe_images(
            Filters=[
                {
                    'Name': 'creation-date',
                    'Values': [
                        f"{parameters['date'].split('-')[1]}-{parameters['date'].split('-')[0]}*",
                    ]
                }
            ],
            Owners=['self'],
        )['Images']

        print("Searching for AMIs...")
        print(ami_list_raw)
        sleep(2)

        # Detecting and building lists of AMIs and their snapshots within the time frame
        ami_to_delete = []
        snapshot_to_delete = []
#        ami_to_delete_report = []

        for ami in ami_list_raw:
            #            ami_creation_datetime = datetime.strptime(ami['CreationDate'], '%Y-%m-%dT%H:%M:%S.%fZ')

            #            if str(ami_creation_datetime.year) == parameters['date'].split('-')[1] and str(ami_creation_datetime.month) == parameters['date'].split('-')[0]:
            print(
                f"AMI {ami['ImageId']} - {ami['Name']} - created {ami['CreationDate']}")
            ami_to_delete.append(ami['ImageId'])
#           ami_to_delete_report.append(ami)

            for ebs in ami['BlockDeviceMappings']:
                if 'Ebs' in ebs:
                    snapshot_id = ebs['Ebs']['SnapshotId']
                    snapshot_to_delete.append(snapshot_id)

        # If AMIs and snapshots lists are not empty, prompt the user
        if ami_to_delete:
            prompt = input(
                "De-register AMIs from the list above and delete associated snapshots? y/n: ")
            if prompt == "y":

                with open('ami_deleted', 'a') as file:
                    writer = csv.writer(file)

                    for ami in ami_list_raw:
                        writer.writerow(
                            [f"AMI {ami['ImageId']} - {ami['Name']} - created {ami['CreationDate']}"])

                    for ami_delete in ami_to_delete:
                        client.deregister_image(ImageId=ami_delete)

                if snapshot_to_delete:
                    for snapshot in snapshot_to_delete:
                        client.delete_snapshot(SnapshotId=snapshot)
                print("Completed.")
        else:
            print("None AMIs to be deleted.")

    print("Exiting...")


ami_list(accounts=accounts, role=role, date=date)
