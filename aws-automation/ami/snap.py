#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : snap.py
# desc    : Searching for snapshots and deleting
# time    : 2023/09/22 15:00:19
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''

import boto3
import csv
from time import sleep

import botocore.exceptions

from vars import *


def snap_list(**parameters):
    # Assuming role to an account from vars file
    for account in parameters['accounts']:
        sts_client = boto3.client('sts')

        print(f"Assuming {parameters['role']} Role to {account} account...")
        sleep(2)

        assumed_role_object = sts_client.assume_role(
            RoleArn=f"arn:aws:iam::{parameters['accounts'][account]}:role/{parameters['role']}",
            RoleSessionName="AssumeRoleSession1")
        credentials = assumed_role_object['Credentials']

        # Fetching snapshot IDs in Sydney region
        client = boto3.client('ec2',
                              region_name='ap-southeast-2',
                              aws_access_key_id=credentials['AccessKeyId'],
                              aws_secret_access_key=credentials['SecretAccessKey'],
                              aws_session_token=credentials['SessionToken'], )
        snap_list_raw = client.describe_snapshots(
            Filters=[
                {
                    'Name': 'start-time',
                    'Values': [
                        f"{parameters['date'].split('-')[1]}-{parameters['date'].split('-')[0]}*",
                    ],
                },
                {
                    'Name': 'owner-id',
                    'Values': [
                        f"{parameters['accounts'][account]}"
                    ],
                },
            ]
        )['Snapshots']
#            Owners=['self'],
#        )['Images']
        print("Searching for snapshots...")
#        print(snap_list_raw)
        sleep(2)

        # Building a list of snapshots to delete
#        snap_to_delete = []
        snapshot_to_delete = []
        #        ami_to_delete_report = []

        for snap in snap_list_raw:
            print(snap)
#            sleep(2)
            #            ami_creation_datetime = datetime.strptime(ami['CreationDate'], '%Y-%m-%dT%H:%M:%S.%fZ')

            #            if str(ami_creation_datetime.year) == parameters['date'].split('-')[1] and str(ami_creation_datetime.month) == parameters['date'].split('-')[0]:
            print(
                f"Snapshot {snap['SnapshotId']} - {snap['Description']} - created {snap['StartTime']}")
            snapshot_to_delete.append(snap['SnapshotId'])
            print(snapshot_to_delete)
#            sleep(10)
            #           ami_to_delete_report.append(ami)

#            for ebs in ami['BlockDeviceMappings']:
#                if 'Ebs' in ebs:
#                    snapshot_id = ebs['Ebs']['SnapshotId']
#                    snapshot_to_delete.append(snapshot_id)

        # If AMIs and snapshots lists are not empty, prompt the user
        if snapshot_to_delete:
            prompt = input("Delete snapshots? y/n: ")
            if prompt == "y":

                with open('snap_deleted', 'a') as file:
                    writer = csv.writer(file)

                    for snap in snap_list_raw:
                        writer.writerow(
                            [f"Snapshot {snap['SnapshotId']} - {snap['Description']} - created {snap['StartTime']}"])

                    for snapshot in snapshot_to_delete:
                        try:
                            client.delete_snapshot(SnapshotId=snapshot)
                        except botocore.exceptions.ClientError as err:
                            status = f"Can't delete a snapshot, {err.response['ResponseMetadata']['HTTPStatusCode']}"
                            print(status)

                print("Completed.")
        else:
            print("No snapshots to be deleted.")

    print("Exiting...")


snap_list(accounts=accounts, role=role, date=date)
