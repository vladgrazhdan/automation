#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : main.py
# desc    : Retreives Datadog agent version for all EC2 instances
# time    : 2023/09/22 15:46:40
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2023, Vlad Grazhdan
# py ver  : 3.10.12
'''


import os
from time import sleep
from datadog_api_client import ApiClient, Configuration
from datadog_api_client.v1.api.hosts_api import HostsApi
from vars import *
os.environ['DD_SITE'] = 'datadoghq.com'


def dd_hosts(**parameters):

    with open('hosts_processed.txt', 'w') as file:
        for customer, keys in parameters['customers'].items():
            print(customer, keys)

            file.write(f'\n{customer}')

            os.environ['DD_API_KEY'] = keys[0]
            os.environ['DD_APP_KEY'] = keys[1]
            configuration = Configuration()
            with ApiClient(configuration) as api_client:
                api_instance = HostsApi(api_client)
                response = api_instance.list_hosts(
                    include_hosts_metadata=True,
                )

                for host in response['host_list']:
                    print(host)

                    if 'agent_version' in host['meta']:
                        if 'azure' in host['apps']:
                            result = [host['id'], host['host_name'],
                                      host['meta']['agent_version']]
                        else:
                            result = [host['host_name'], host['host_name'],
                                      host['meta']['agent_version']]
                    elif not host['apps']:
                        result = 'Unidentified resource'
                    elif host['apps'][0] not in parameters['services'] and 'aws_id' in host:
                        result = [host['aws_id'], host['host_name'],
                                  'Unknown agent version']
                    elif host['apps'][0] in parameters['services']:
                        result = f"{host['apps'][0]}"
                    else:
                        result = 'Unidentified resource'

                    print(result)

                    file.write(f'\n{result}')

                    sleep(2)

    file.close()


dd_hosts(customers=customers, services=service_type)
