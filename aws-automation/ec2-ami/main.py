#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : main.py
# desc    : Creates AMI from EC2 instances
# time    : 2023/09/22 15:29:48
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''

import boto3
from time import sleep
from datetime import datetime
from vars import *


def group(**parameters):

    for account in parameters['accounts']:

        print(f"Assuming {role} Role to {account} account...")

        sleep(2)
        sts_client = boto3.client('sts')

        assumed_role_object = sts_client.assume_role(
            RoleArn=f"arn:aws:iam::{parameters['accounts'][account]}:role/{parameters['role']}",
            RoleSessionName="AssumeRoleSession1"
        )
        credentials = assumed_role_object['Credentials']

        for region in parameters['regions']:
            print(
                f"Detecting EC2 instances in {account} account in {parameters['regions'][region]} region...")
            client = boto3.resource('ec2',
                                    region_name=parameters['regions'][region],
                                    aws_access_key_id=credentials['AccessKeyId'],
                                    aws_secret_access_key=credentials['SecretAccessKey'],
                                    aws_session_token=credentials['SessionToken'],
                                    )
            group_list = client.instances.filter(
                Filters=[
                    {
                        'Name': [key for key in parameters['tags']][0],
                        'Values': [parameters['tags'][key][0] for key in parameters['tags']]
                    }
                ]
            )

            # Creating instances backup
            if group_list:
                for instance in group_list:
                    instance_name = [name['Value']
                                     for name in instance.tags if name['Key'] == 'Name']
                    ami = client.Instance(instance.id).create_image(
                        Name=f"Backup-{datetime.now():%d-%m-%Y}-{instance.id}-{instance_name[0]}",
                        Description=f"Backup AMI for {instance.id} {instance_name[0]}",
                        NoReboot=True,
                        TagSpecifications=[
                            {
                                'ResourceType': 'image',
                                'Tags': [
                                    {
                                        'Key': 'Name',
                                        'Value': f'{instance_name[0]}'
                                    },
                                ]
                            },
                        ]
                    )
                    print(f"Creating AMI for {instance.id} {instance_name[0]}")

#                   ami.wait_until_exists(
#                       Filters=[
#                           {
#                               'Name': 'state',
#                               'Values': ['available']
#                           }
#                       ]
#                   )
    print('Exiting...')


group(accounts=group_accounts, regions=group_regions, role=role, tags=tags)
