#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : ec2_automation.py
# desc    : Launch EC2 instance. Install Docker and Ngingx
# time    : 2023/09/22 15:06:33
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''

import boto3
import time
import paramiko
import requests
import schedule
from vars import *

ec2_resource = boto3.resource('ec2', region_name = 'ap-southeast-2')
ec2_client = boto3.client('ec2', region_name = 'ap-southeast-2')

# Starting EC2 instance in the default VPC

# Checking if the instance already exists
response = ec2_client.describe_instances(
    Filters=[
        {
            'Name': 'tag:Name',
            'Values': [
                'main-server',
            ]
        },
    ]
)

instance_exists = len(response["Reservations"]) != 0 and len(response["Reservations"][0]["Instances"]) != 0
print(instance_exists)
instance_id = ''

if not instance_exists:
    print('Creating a new ec2 instance...')
    ec2_creation_result = ec2_resource.create_instances(
        ImageId=image_id,
        KeyName=key_name,
        MinCount=1,
        MaxCount=1,
        InstanceType=instance_type,
        TagSpecifications=[
            {
                'ResourceType': 'instance',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': 'main-server'
                    },
                ]
            },
        ],

    )
    instance = ec2_creation_result[0]
    instance_id = instance.id
else:
    instance = response["Reservations"][0]["Instances"][0]
    instance_id = instance["InstanceId"]
    print('Instance already exists')


# Waiting until the EC2 server is fully initialized
ec2_instance_fully_initialised = False

while not ec2_instance_fully_initialised:
    print('Getting instance status...')
    statuses = ec2_client.describe_instance_status(
        InstanceIds = [instance_id]
    )
    if len(statuses['InstanceStatuses']) != 0:
        ec2_status = statuses['InstanceStatuses'][0]

        ins_status = ec2_status['InstanceStatus']['Status']
        sys_status = ec2_status['SystemStatus']['Status']
        state = ec2_status['InstanceState']['Name']
        ec2_instance_fully_initialised = ins_status == 'ok' and sys_status == 'ok' and state == 'running'
    if not ec2_instance_fully_initialised:
        print('Waiting for 30 seconds...')
        time.sleep(30)

print('Instance is fully initialised')

# Getting the instance's public ip address
response = ec2_client.describe_instances(
    Filters=[
        {
            'Name': 'tag:Name',
            'Values': [
                'main-server',
            ]
        },
    ]

)
print(response)
instance = response["Reservations"][0]["Instances"][0]
print(instance)
ssh_host = instance["PublicIpAddress"]

# Installing Docker on the EC2 server & starting nginx container...

commands_to_execute = [
    'sudo yum update -y && sudo yum install -y docker',
    'sudo systemctl start docker',
    'sudo usermod -aG docker ec2-user',
    'docker run -d -p 8080:80 --name nginx nginx'
]

# Connecting to the EC2 server...
print('Connecting to the server...')
print(f"Public ip: {ssh_host}")
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(hostname=ssh_host, username=ssh_user, key_filename=ssh_private_key_path)

# Installing Docker, starting nginx
for command in commands_to_execute:
    stdin, stdout, stderr = ssh.exec_command(command)
    print(stdout.readlines())

ssh.close()

# Opening port 8080 on the nginx server, if not already open
sg_list = ec2_client.describe_security_groups(
    GroupNames=['default']
)

port_open = False
for permission in sg_list['SecurityGroups'][0]['IpPermissions']:
    print(permission)
    # Some permissions don't have FromPort set
    if 'FromPort' in permission and permission['FromPort'] == 8080:
        port_open = True

if not port_open:
    sg_response = ec2_client.authorize_security_group_ingress(
        FromPort=8080,
        ToPort=8080,
        GroupName='default',
        CidrIp='0.0.0.0/0',
        IpProtocol='tcp'
    )

app_not_accessible_count = 0

def restart_container():
    print('Restarting the application...')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=ssh_host, username=ssh_user, key_filename=ssh_private_key_path)
    stdin, stdout, stderr = ssh.exec_command('docker start nginx')
    print(stdout.readlines())
    ssh.close()
    # Resetting the count
    global app_not_accessible_count
    app_not_accessible_count = 0

    print(app_not_accessible_count)

def monitor_application():
    global app_not_accessible_count
    try:
        response = requests.get(f"http://{ssh_host}:8080")
        if response.status_code == 200:
            print('Application is running successfully!')
        else:
            print('Application is offline.')
            app_not_accessible_count += 1
            if app_not_accessible_count == 5:
                restart_container()
    except Exception as ex:
        print(f'Connection error: {ex}')
        print('Cannot access the app')
        app_not_accessible_count += 1
        if app_not_accessible_count == 5:
            restart_container()
        return "test"


schedule.every(10).seconds.do(monitor_application)

while True:
    schedule.run_pending()