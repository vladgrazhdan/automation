import boto3

ec2_client = boto3.client('ec2', region_name = 'ap-southeast-2')
iam_client = boto3.client('iam', region_name = 'ap-southeast-2')

subnets = ec2_client.describe_subnets()
for subnet in subnets['Subnets']:
    print(f"Subnet ID is {subnet['SubnetId']}")

iam_users = iam_client.list_users()
# latest_user_active1 = iam_users['Users'][0]
# latest_user_active2 = iam_users['Users'][1]
latest_user_active = iam_users['Users'][0]

for iam_user in iam_users['Users']:
    print(f"User {iam_user['UserName']} was last active on {iam_user['PasswordLastUsed']}")
    if latest_user_active['PasswordLastUsed'] < iam_user['PasswordLastUsed']:
        latest_user_active = iam_user

print(f'The user who was active recently was {latest_user_active["UserName"]}')