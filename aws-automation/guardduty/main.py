#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : main.py
# desc    : Lists GuardDuty accounts 
# time    : 2023/09/22 15:36:27
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2023, Vlad Grazhdan
# py ver  : 3.10.12
'''

import boto3
from vars import *


def guard_duty(**parameters):
    aws_session = boto3.Session()
    mfa_serial = aws_session._session.full_config['profiles']['default']['mfa_serial']
    mfa_code = input('Please enter your MFA code: ')

    sts = aws_session.client('sts')
    mfaToken = sts.get_session_token(SerialNumber=mfaSerial, TokenCode=mfaCode)

    sts_client = boto3.client('sts')
    assumed_role_object = sts_client.assume_role(
        RoleArn=f"arn:aws:iam::{account1}:role/ReadOnly",
        RoleSessionName="AssumeRoleSession1"
    )
    credentials = assumed_role_object['Credentials']

    print(f"Assumed role for {account1}...")

    sts = awsSession.client('sts')
    mfaToken = sts.get_session_token(SerialNumber=mfaSerial, TokenCode=mfaCode)

    iam = awsSession.client('iam',
                            aws_session_token=mfaToken['Credentials']['SessionToken'],
                            aws_secret_access_key=mfaToken['Credentials']['SecretAccessKey'],
                            aws_access_key_id=mfaToken['Credentials']['AccessKeyId']
                            )
    userList = iam.list_users()


#    for region in parameters['regions']:
#        client = boto3.client('guardduty',
#        region_name=region,
#        aws_access_key_id=credentials['AccessKeyId'],
#        aws_secret_access_key=credentials['SecretAccessKey'],
#        aws_session_token=credentials['SessionToken'],
#        )
#        guard_duty_list_raw = client.list_members()
#        print(guard_duty_list_raw)
    # guard_duty_list = []
    # for gd in guard_duty_list_raw['Users']:
    #     users_list.append(user['UserName'])
    # writer.writerow([users_list])
guard_duty(regions=regions)
