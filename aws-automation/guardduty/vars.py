account1 = '11111111111'
account2 = '11111111111'

regions = {
    'ap-northeast-3': 'Osaka',
    'ap-northeast-2': 'Seoul',
    'ap-southeast-1': 'Singapore',
    'ap-northeast-1': 'Tokyo',
    'ca-central-1': 'Canada',
    'eu-central-1': 'Frankfurt',
    'eu-west-1': 'Ireland',
    'eu-west-2': 'London',
    'eu-west-3': 'Paris',
    'eu-north-1': 'Stockholm',
    'sa-east-1': 'Sao Paulo',
    'us-east-1': 'N. Virginia',
    'us-east-2': 'Ohio',
    'us-west-1': 'N. California'
}
