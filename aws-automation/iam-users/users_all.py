#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : users_all.py
# desc    : Fetch IAM usernames and stores in a CSV file
# time    : 2023/09/22 15:11:21
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''


import boto3
import csv
from time import sleep
from vars import *


def iam_users_list(**parameters):

    # Assuming role to accounts, creating a csv file
    sts_client = boto3.client('sts')
    with open(parameters['filename'], 'a', encoding='utf8') as csvfile:
        writer = csv.writer(csvfile, delimiter='\t')
        for acc_name in parameters['accounts']:

            print(f"Assuming role to {acc_name} account...")
            sleep(1)

            writer.writerow([f" list of IAM USERS in {acc_name} account "])
            assumed_role_object = sts_client.assume_role(
                RoleArn=f"arn:aws:iam::{parameters['accounts'][acc_name]}:role/ReadOnly",
                RoleSessionName="AssumeRoleSession1"
            )
            credentials = assumed_role_object['Credentials']

            # Fetching users and creating IAM users list
            client = boto3.client('iam',
                                  aws_access_key_id=credentials['AccessKeyId'],
                                  aws_secret_access_key=credentials['SecretAccessKey'],
                                  aws_session_token=credentials['SessionToken'],
                                  )
            users_list_raw = client.list_users()
            users_list = []

            # Writing IAM user names to the csv file
            for user in users_list_raw['Users']:
                users_list.append(user['UserName'])
            writer.writerow([users_list])

            print(
                f"Adding IAM users from {acc_name} account to {parameters['filename']}...")
            sleep(1)

    print("Completed. Exiting...")


iam_users_list(filename='iam-users.csv', accounts=comestri_accounts)
