from wsgiref.util import request_uri
import requests
import time
import os

ssh_host = os.environ['EC2_SERVER']
host_port = os.environ['HOST_PORT']

# Validating the app status
try:
    # Waiting for the app to start
    time.sleep(15)
    
    response = requests.get(f"http://{ssh_host}:{host_port}")
    if response.status_code == 200:
        print('The App is running')
    else:
        print('Application failed')
except Exception as ex:
    print(f'Connection error: {ex}')
    print('Application failed')