#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : main.py
# desc    : Iterates over all EC2 instances and runs SSM commands to retrieve data from EC2 instances 
# time    : 2023/09/22 15:17:36
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2023, Vlad Grazhdan
# py ver  : 3.10.12
'''


import boto3
import botocore
from vars import *
from time import sleep

ec2list = []


def assume_role_with_mfa(role_arn):

    sts_client = boto3.client('sts',
                              region_name='ap-southeast-2',
                              endpoint_url='https://sts.ap-southeast-2.amazonaws.com',
                              )
    mfa = input("Enter MFA code: ")
    assumed_role_object = sts_client.assume_role(
        RoleArn=role_arn,
        RoleSessionName='newsession',
        SerialNumber="arn:aws:iam::111111111111:mfa/bob.bobby",
        DurationSeconds=3600,
        TokenCode=mfa
    )

    return assumed_role_object['Credentials']


def ec2_list(**parameters):

    # Iterate over customers accounts
    for customer in parameters['customers']:
        for account in parameters['customers'][customer].keys():
            print(f"Assuming {role} role to {customer} {account} account...")
            sleep(1)

            # Uncomment for the MFA based accounts
            credentials = assume_role_with_mfa(
                role_arn=f"arn:aws:iam::{parameters['customers'][customer][account]}:role/{parameters['role']}")

            # Assume Admin role to run SSM commands later. Comment for the MFA based accounts
#            sts_client_admin = boto3.client('sts',
#                                            region_name='ap-southeast-2',
#                                            endpoint_url='https://sts.ap-southeast-2.amazonaws.com',
#                                            )
#            assumed_role_object_admin = sts_client_admin.assume_role(
#                RoleArn=f"arn:aws:iam::{parameters['customers'][customer][account]}:role/{parameters['role']}",
#                RoleSessionName="AssumeRoleSession2"
#            )
#            credentials = assumed_role_object_admin['Credentials']

            # Get a list of regions
            ec2 = boto3.client('ec2',
                               aws_access_key_id=credentials['AccessKeyId'],
                               aws_secret_access_key=credentials['SecretAccessKey'],
                               aws_session_token=credentials['SessionToken'],
                               )
            regions = ec2.describe_regions().get('Regions', [])

            # Iterate over regions
            for region in regions:
                print("* Checking region - %s " % region['RegionName'])
                reg = region['RegionName']

                client = boto3.client('ec2',
                                      region_name=reg,
                                      aws_access_key_id=credentials['AccessKeyId'],
                                      aws_secret_access_key=credentials['SecretAccessKey'],
                                      aws_session_token=credentials['SessionToken'],
                                      )

                response = client.describe_instances()

                # Iterate over instances in a region
                for reservation in response['Reservations']:
                    for instance in reservation['Instances']:

                        instance_name = [k['Value'] for k in instance['Tags']
                                         if k['Key'] == 'Name'] if 'Tags' in instance else ['NoName']
                        instance_name = instance_name[0] if instance_name else ''

                        print("  - Instance %s %s %s %s" % (instance['InstanceId'],
                                                            instance_name,
                                                            instance['State']['Name'],
                                                            instance['PlatformDetails']))

                        if instance['State']['Name'] != 'running' and instance['PlatformDetails'] not in exclude_platform:
                            ec2list.append({'customer': customer,
                                            'account_name': account,
                                            'account_id': parameters['customers'][customer][account],
                                            'region': reg,
                                            'instance_id': instance['InstanceId'],
                                            'instance_name': instance_name,
                                            'state': instance['State']['Name'],
                                            'platform': instance['PlatformDetails'],
                                            'gpg_key': '',
                                            'repo_yum': '',
                                            'repo_zypp': ''})

                        # Run SSM commands for non-Win OS in the running state only
                        elif instance['PlatformDetails'] not in exclude_platform:

                            # Run SSM commands
                            ssm_client = boto3.client('ssm',
                                                      region_name=reg,
                                                      aws_access_key_id=credentials['AccessKeyId'],
                                                      aws_secret_access_key=credentials['SecretAccessKey'],
                                                      aws_session_token=credentials['SessionToken'],
                                                      )

                            try:
                                # Check if host trusts the GPG key
                                response_gpg_key = ssm_client.send_command(
                                    InstanceIds=[instance['InstanceId']],
                                    DocumentName="AWS-RunShellScript",
                                    Parameters={'commands': [
                                        'rpm -q gpg-pubkey-4172a230-55dd14f6']},
                                )

                                print(
                                    "Running SSM commands for this instance... Please wait...")

                                sleep(3)

                                command_id = response_gpg_key['Command']['CommandId']
                                output_gpg_key = ssm_client.get_command_invocation(
                                    CommandId=command_id,
                                    InstanceId=instance['InstanceId']
                                )
                                output_gpg_key = f"{output_gpg_key['ResponseCode']}, {output_gpg_key['Status']}, " \
                                                 f"{output_gpg_key['StandardErrorContent']}"
                                print(output_gpg_key)

                                # Check the Datadog repo file
                                response_repo_yum = ssm_client.send_command(
                                    InstanceIds=[instance['InstanceId']],
                                    DocumentName="AWS-RunShellScript",
                                    Parameters={
                                        'commands': ['grep "DATADOG_RPM_KEY.public" /etc/yum.repos.d/datadog.repo']},
                                )

                                sleep(3)

                                command_id = response_repo_yum['Command']['CommandId']
                                output_repo_yum = ssm_client.get_command_invocation(
                                    CommandId=command_id,
                                    InstanceId=instance['InstanceId']
                                )
                                output_repo_yum = f"{output_repo_yum['ResponseCode']}, {output_repo_yum['Status']}, " \
                                                  f"{output_repo_yum['StandardErrorContent']}"
                                print(output_repo_yum)

                                response_repo_zypp = ssm_client.send_command(
                                    InstanceIds=[instance['InstanceId']],
                                    DocumentName="AWS-RunShellScript",
                                    Parameters={
                                        'commands': ['grep "DATADOG_RPM_KEY.public" /etc/zypp/repos.d/datadog.repo']},
                                )

                                sleep(3)

                                command_id = response_repo_zypp['Command']['CommandId']
                                output_repo_zypp = ssm_client.get_command_invocation(
                                    CommandId=command_id,
                                    InstanceId=instance['InstanceId']
                                )
                                output_repo_zypp = f"{output_repo_zypp['ResponseCode']}, {output_repo_zypp['Status']}, " \
                                                   f"{output_repo_zypp['StandardErrorContent']}"
                                print(output_repo_zypp)

                            except botocore.exceptions.ClientError as err:
                                status = f"Check SSM agent, {err.response['ResponseMetadata']['HTTPStatusCode']}"

                                output_gpg_key = status
                                output_repo_yum = status
                                output_repo_zypp = status
                                print(output_gpg_key)

                            ec2list.append({'customer': customer,
                                            'account_name': account,
                                            'account_id': parameters['customers'][customer][account],
                                            'region': reg,
                                            'instance_id': instance['InstanceId'],
                                            'instance_name': instance_name,
                                            'state': instance['State']['Name'],
                                            'platform': instance['PlatformDetails'],
                                            'gpg_key': f'{output_gpg_key}',
                                            'repo_yum': f'{output_repo_yum}',
                                            'repo_zypp': f'{output_repo_zypp}'})

    print(ec2list)

    # Writing the result into a csv file
    with open(f"hosts_processed.csv", 'w') as file:
        file.writelines(str(['Customer', 'Account Name', 'Account Number', 'Region', 'Instance ID',
                        'Instance Name', 'Status', 'Platform', 'GPG key', 'YUM repo', 'ZYpp repo'])[1:-1])
        for item in ec2list:
            print(str(list(item.values()))[1:-1])
            file.writelines(f'\n{str(list(item.values()))[1:-1]}')

    file.close()


ec2_list(role=role, customers=customer)
