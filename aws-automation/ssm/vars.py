role = 'Admin'

exclude_platform = ['Windows', 'Windows with SQL Server Standard']

customer1 = {
    'mech': {'account1': '111111111111'}
}

customer2 = {
    'name1': {'account1': '111111111111',
              'account2': '111111111111'},
    'name2': {'account1: '111111111111',
              'account2': '111111111111'}
}
