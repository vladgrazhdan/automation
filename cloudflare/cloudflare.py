#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : cloudflare.py
# desc    : Manage DNS records via Cloudflare API
# time    : 2023/09/09 19:55:16
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''


import re, requests, json

API_ENDPOINT = 'https://api.cloudflare.com/client/v4/'
API_USER = ''
API_TOKEN = ''

ZONE_ID = ''

def add_dnsrecord():
	'''Adding A record'''

	#context()

	#range_source = active_sheet.getCellRangeByPosition(0, 0, 1, 20)
	#data_source = range_source.getDataArray()
	#pbx_config = dict(zip(data_source[0],data_source[1]))
	#pbx_config = dict(data_source)
	
	#hostname = re.sub('[\W_]', '',pbx_config['domain_name'])
	#domain_name = hostname.lower() + ".pbx.localtelco.co"
	#headers = {'Authorization': 'Bearer ' + API_TOKEN,'Content-Type':'application/json'}
	headers = {'Authorization': 'Bearer ' + API_TOKEN}
	#r = requests.get(url = API_ENDPOINT + 'zones/' + ZONE_ID + '/dns_records', headers = headers)
	r = requests.post(url = API_ENDPOINT + 'zones/' + ZONE_ID + '/dns_records', data = json.dumps({'type':'A','name':'testautomation'+'.pbx','content':'175.178.126.119','ttl':'600','proxied':False}), headers = headers) 
	#r = requests.get(url = API_ENDPOINT + 'zones/' + ZONE_ID, headers = headers)
	#r = requests.get(url = API_ENDPOINT + 'zones', headers = headers)
	print(r, r.status_code, r.content)

	#if (r.status_code != 200):
	#	payload = [domain_name,hostname.lower()]
	#	r = requests.post(url = API_ENDPOINT + URL_CREATE, data = json.dumps(payload),auth = (API_USER,API_KEY))
	#	new_domain = True
	#pbx_config.pop('domain_name')
	#r = requests.post(url = API_ENDPOINT + URL_UPDATE + domain_name + '/config', data = json.dumps(pbx_config), auth = (API_USER,API_KEY))
	#message_button(new_domain, r.status_code, 'Domain')

add_dnsrecord()