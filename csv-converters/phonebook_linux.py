#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : phonebook_linux.py
# desc    : Creates a phonebook file for Yealink
# time    : 2023/09/09 19:59:53
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''


from os import path, getcwd, listdir
import csv

def csv_filename():
	file_list = listdir(getcwd())
	for file in file_list:
		if file.endswith(".csv"):
			return parse_csv(file)
	print ("no CSV file has been found")
	return

def parse_csv(csv_file):
	with open (csv_file,'r') as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=',')
		next(csv_reader)
		csv_reader = list(csv_reader)
		return convert_to_xml(csv_reader)

def convert_to_xml(csv_reader):
	with open ('phonebook.xml','w') as xml_file:
		xml_file.write('<?xml version="1.0" encoding="UTF-8"?>' + "\n")
		xml_file.write('<YealinkIPPhoneBook>' + "\n")
		xml_file.write('   <Title>Yealink</Title>' + "\n")
		xml_file.write('   <Menu Name="Clients">' + "\n")
		omit = ['6','1','0']
		for i in csv_reader:
			for index in range(1,4):
				if len(i[index]) > 0 and i[index][0] not in omit:
					i[index] = (i[index].rjust(1 + len(i[index]),'0'))
			xml_file.write(f'      <Unit Name="{i[0]}" Phone1="{i[1]}" Phone2="{i[2]}" Phone3="{i[3]}"/>' + "\n")
		xml_file.write('   </Menu>' + "\n")
		xml_file.write('</YealinkIPPhoneBook>')
	return

csv_filename()