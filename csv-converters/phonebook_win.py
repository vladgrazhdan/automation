#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : phonebook_win.py
# desc    : Converts CSV file to a phonebook file for Yealink (Windows OS version)
# time    : 2023/09/09 20:00:45
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''


import csv,os

os.chdir(os.path.dirname(__file__))
print(os.getcwd())

xml_file = 'phonebook.xml'
csv_file = 'phonebook.csv'

csv_data = csv.reader(open(csv_file))
next(csv_data)
xml_data = open(xml_file, 'w')

xml_data.write('<?xml version="1.0" encoding="UTF-8"?>' + "\n")
xml_data.write('<YealinkIPPhoneBook>' + "\n")
xml_data.write('   <Title>Yealink</Title>' + "\n")
xml_data.write('   <Menu Name="Clients">' + "\n")

omit = ['6','1','0']
for i in csv_data:
	for index in range(1,4):
		if len(i[index]) > 0 and i[index][0] not in omit:
			i[index] = (i[index].rjust(1 + len(i[index]),'0'))
	xml_data.write(f'      <Unit Name="{i[0]}" Phone1="{i[1]}" Phone2="{i[2]}" Phone3="{i[3]}"/>' + "\n")

xml_data.write('   </Menu>' + "\n")
xml_data.write('</YealinkIPPhoneBook>')

xml_data.close()