#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : xml_to_csv_converter.py
# desc    : XML to CSV converter
# time    : 2023/09/09 20:01:32
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''

from os import path, getcwd, listdir
import csv, re

def xml_filename():
	file_list = listdir(getcwd())
	for file in file_list:
		if file.endswith(".xml"):
			return parse_xml(file)
	print ("no XML file has been found")
	return

def parse_xml(xml_file):
	with open (xml_file,'r') as xml_file:
		xml_reader = xml_file.readlines()
		xml_reader = list(xml_reader)
		new_file = {}
		for i in xml_reader:
			if '<Unit Name=' in i:
				name = re.findall('"\D+" ',i)
				new_name = name[0].replace('" ','').replace('"','')
				phone_number = re.findall('(\d{10})', i)
				new_file[new_name] = phone_number[0]
		return convert_to_csv(new_file)

def convert_to_csv(new_file):
	with open ('phonebook.csv','w') as csv_file:
		for k,v in new_file.items():
			if ' ' in k:
				k = k.split(" ", 1)
				csv_file.write(k[0] + ';' + k[1] + ';' + ';' + v + ';'+ '\n')
			else:
				csv_file.write(k + ';' + ';' + ';' + v + ';'+ '\n')
	return

xml_filename()