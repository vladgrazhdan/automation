#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
# file    : tuple_to_dict.py
# desc    : Tuple to dictionary converter for Vodia API
# time    : 2023/09/09 20:02:20
# author  : Vlad Grazhdan
# version : 1.0
# contact : volodymyr.grazhdan@gmail.com
# license : (C)Copyright 2022, Vlad Grazhdan
# py ver  : 3.10.12
'''


import re, requests, json

tupl = (('domain_name', 'max_extensions', 'max_calls', 'max_trunk_calls', 'admin_name', 'admin_email', 'admin_password', 'admin_phone'), ('TestAutomationTenant', 101.0, 10.0, 10.0, 1.0, 2.0, 3.0, 4.0))

d1 = dict(zip(tupl[0],tupl[1]))
print(d1)

hostname=re.sub('[^a-zA-Z0-9 \n\.]', '',d1['domain_name'])
alias=hostname.replace(" ", "")
domain_name=alias.lower()+".pbx.test.co"
payload=[domain_name,alias.lower()]
URL_PATH="rest/system/domains"
API_URL='https://pbx.test.co/' + URL_PATH
print(payload)
r = requests.post(url=API_URL, data=json.dumps(payload),auth=('',''))
print(r.status_code)
    #if(r.status_code == 200):
    #    return True
    #else:
    #    return False
