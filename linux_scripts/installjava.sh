#!/bin/bash
#
# This script checks installed Java version 11.0.11
#
# Copyright 2021 Vlad Grazhdan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


read -p "Install Java? (y/n) " choice
if [ $choice = "y" ]; then
   echo "Checking installed Java version..."
   version=$(java -version 2>&1 | awk -F'"' '{print $2}')
   if [ $version = "11.0.11" ]; then	   
      echo "Java version $version is already installed"
   else 
      echo "Installing Java..."
      sudo apt install default-jre
      version=$(java -version 2>&1 | awk -F'"' '{print $2}')
      if [ $version = "11.0.11" ]; then
         echo "Java $version has been successfully installed"
      else "Java installation failed"
      fi
   fi
else
   echo "OK. Bye."
fi

