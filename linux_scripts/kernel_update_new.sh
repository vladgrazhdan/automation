#!/bin/bash
#
# This script is aimed to mitigate Retbleed vulnerability
# (CVE-2022-29900 / CVE-2022-23816, CVE-2022-29901, CVE-2022-23825).
# The script checks distribution type and Linux kernel version. 
# Based on these, it gives a recommendation on the Linux kernel update.
#
# Copyright 2022 Vlad Grazhdan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#Detect the distro version
distro_ver() {
  echo "Detecting the Linux distribution version..."
  [ -f /etc/os-release ] && { distro=$(cat /etc/os-release|grep "\<NAME\>"|awk -F'=' {'print $2'}); echo ""${distro}" has been detected..."; } || { echo "Cannot detect the Linux distribution."; exit 0; }

  if [[ "$distro" == '"Amazon Linux"' ]]; then
    kernel_update "5.15.57"
  elif [[ "$distro" == '"Ubuntu"' ]]; then
    distro_ver=$(cat /etc/os-release|grep "\<VERSION_ID\>"|awk -F'=' {'print $2'})
    if [[ "$distro_ver" == '"20.04"' ]] || [[ "$distro_ver" == '"22.04"' ]]; then
      echo "Ubuntu "${distro_ver}" has been detected. No action is required."
    else echo "Ubuntu "${distro_ver}" has been detected. The vulnerability will not be fixed. OS upgrade is required!"
    fi
  elif [[ "$distro" == '"Red Hat Enterprise Linux"' ]]; then
    distro_ver=$(cat /etc/os-release|grep "\<VERSION_ID\>"|awk -F'=' {'print $2'}|sed -e 's/"\(.*\).../\1/')
      if [[ "$distro_ver" == '7' ]] || [[ "$distro_ver" == '"9"' ]]; then
        echo "RHEL version "${distro_ver}" has been detected. This version is affected and will receive a fix in the near future. No action is required."
      elif [[ "$distro_ver" == '8' ]]; then
        kernel_update "4.18.0"
      else echo "RHEL "${distro_ver}" has been detected. The vulnerability will not be fixed. OS upgrade is required!"
      fi  
  else echo "Cannot detect the Linux distribution version. Exiting now..."
  fi
}

#Upgrade the kernel
kernel_update() {
  desired_kernel_ver=$1
  current_kernel_ver=$(uname -r|awk -F'-' {'print $1'})
  echo "Linux kernel "${current_kernel_ver}" is detected..."
  echo "Recommended Linux kernel is "${desired_kernel_ver}""
  kernel_version $current_kernel_ver $desired_kernel_ver && upgrade_required=true || upgrade_required=false

  if [[ $upgrade_required = true ]]; then
    if [[ "$distro" == '"Amazon Linux"' ]]; then
    sudo amazon-linux-extras install kernel-5.15 -y
    read -p "Reboot is required to boot with the new kernel. Reboot now? y/n: " user_input
    [[ "$user_input" == 'y' ]] && { echo "Rebooting now..."; sudo reboot; } || { echo "Exiting now..."; exit 0; }
    elif [[ "$distro" == '"Red Hat Enterprise Linux"' ]]; then
      echo "The kernel update is required. It needs to be run manually on a case-by-case basis." 
    fi

  else
    echo "Kernel upgrade is not required."
    exit 0
  fi
}

#Detect whether the kernel upgrade is required or not
kernel_version() {
  [ "$1" = "$2" ] && return 1 || kernel_version_e $1 $2
}
kernel_version_e() {
  [  "$1" = "`echo -e "$1\n$2" | sort -V | head -n1`" ]
}

distro_ver
