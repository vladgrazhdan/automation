#!/bin/bash
#
# This script checks Node.js PID and listening port
#
# Copyright 2021 Vlad Grazhdan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#node_status=$(ps aux 2>&1 | grep node | head -n 1 | awk '{ print $11}')

#echo "Node status is $(awk -v var="$node_status" 'BEGIN { print var }' | awk '{ print $11}')"

#if [ $node_status = "node" ]; then
#   node_pid=$(ps aux 2>&1 | grep node | head -n 1 | awk '{print $2}')
#   node_port=$(netstat -tulpan 2>/dev/null | grep node | awk -F':::' '{print $2}')
#   echo "Node.js PID $node_pid is running and listening on port $node_port"
#else
#   echo "Node.js is not running"
#fi

node_status=$(pgrep -f server.js)
if [[ "$node_status" != "" ]]; then
   node_port=$(sudo netstat -tulpan 2>/dev/null | grep node | awk -F':::' '{print $2}')
   echo "Node.js PID "${node_status}" is running and listening on port "${node_port}""
else
   echo "Node.js is not running"
fi


