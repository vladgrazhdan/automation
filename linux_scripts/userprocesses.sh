#!/bin/bash
#
# This script shows running processes sorted by CPU/memory consumption.
#
# Copyright 2021 Vlad Grazhdan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


read -p "Would you like the output to be sorted by memory or CPU consumption? (n) no, (m) memory, (c) CPU, (q) quit: " option

while ! [[ "$option" =~ ^[nmcq]$ ]] 
do
   read -p "Would you like the output to be sorted by memory or CPU consumption? (n) no, (m) memory, (c) CPU, (q) quit: " option
done

if [[ "$option" == "q" ]]; then 
   echo "OK. Bye." 
   exit 0 
fi

read -p "How many processes to show? (a) all or enter a number: " processes

while ! [[ "$processes" =~ ^[1-9][1-9]*$ || "$processes" == "a" ]]
do
   read -p "How many processes to show? (a) all or enter a number: " processes
done   

user=$(printenv USER)

if [[ "$processes" == "a" ]]; then
   if [[ "$option" == "n" ]]; then
      echo "$(ps aux | (head -n 1; grep ^$user))"
   elif [[ "$option" == "m" ]]; then
      echo "$(ps aux --sort -pmem | (head -n 1; grep ^$user))"
   elif [[ "$option" == "c" ]]; then
      echo "$(ps aux --sort -pcpu | (head -n 1; grep ^$user))"
   fi
else
   if [[ "$option" == "n" ]]; then
      echo "$(ps aux | (head -n 1; grep ^$user) | head -n $((processes+1)))"
   elif [[ "$option" == "m" ]]; then
      echo "$(ps aux --sort -pmem | (head -n 1; grep ^$user) | head -n $((processes+1)))"
   elif [[ "$option" == "c" ]]; then
      echo "$(ps aux --sort -pcpu | (head -n 1; grep ^$user) | head -n $((processes+1)))"
   fi
fi